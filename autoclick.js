const startKeybind = document.querySelector('#startKeybind');
const stopKeybind = document.querySelector('#stopKeybind');
const status = document.getElementById('status');

// Autoclicker state
let timeoutId; 

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function loadDefault() {
  if (timeoutId) {
    stopAutoClicker();
  } else {
    status.innerText = 'Autoclicker Off';
  }
}

async function startAutoClicker() {
  const time = prompt('How frequent should the autoclicker be in milliseconds?');
  
  let delay = parseInt(time, 10);

  if (isNaN(delay)) {
    alert('Invalid input. Time must be a number.');
    return;
  }
  
  try {
    status.innerText = 'Autoclicker On';

    await sleep(500);

    function click() {
      document.dispatchEvent(new MouseEvent('click'));
      timeoutId = setTimeout(click, delay);
    }

    timeoutId = setTimeout(click, delay);
  } catch (err) {
    console.error(err);
    status.innerText = 'Error starting autoclicker';
  }
}

function stopAutoClicker() {
  status.innerText = 'Autoclicker Off';
  clearTimeout(timeoutId);
}

// Start autoclicker on keybind
startKeybind.addEventListener('keydown', () => {
  if (!timeoutId) {
    startAutoClicker();
  } else {
    status.innerText = 'Error: Autoclicker already enabled';
    console.error('Autoclicker already enabled');
    
    sleep(500).then(() => {
      status.innerText = 'Autoclicker On';
    });
  }
});

// Stop autoclicker on keybind
stopKeybind.addEventListener('keydown', () => {
  stopAutoClicker(); 
});

loadDefault();
